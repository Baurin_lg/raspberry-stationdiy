void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(115200);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int analog0 = analogRead(A0);
  int analog1 = analogRead(A1);
  int analog2 = analogRead(A2);
  int analog3 = analogRead(A3);
  int analog4 = analogRead(A4);
  int analog5 = analogRead(A5);
  // print out the value you read:
  Serial.print("{ 'A0':");
  Serial.print(analog0);
  Serial.print(",'A1':");
  Serial.print(analog1);
  Serial.print(",'A2':");
  Serial.print(analog2);
  Serial.print(",'A3':");
  Serial.print(analog3);
  Serial.print(",'A4':");
  Serial.print(analog4);
  Serial.print(",'A5':");
  Serial.print(analog5);
  Serial.println("}");
  delay(1);        // delay in between reads for stability
  
}