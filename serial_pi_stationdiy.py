
import time
import serial
import threading
import RPi.GPIO as GPIO
from stationdiy import StationDiY




class Importer():

    def readSerialPortThread(self):
        """ Read port """
        self.baudrate=115200
        self.pathport = '/dev/ttyAMA0'
        self.arduino=serial.Serial(self.pathport,baudrate=self.baudrate, timeout = 5.0)

        value_check = False
        while value_check == False:
            try:
                self.arduino.open()
                line =  self.arduino.readline()
                line_converse = eval(line)
                self.arduino.close()
                value_check = True

            except Exception as e:
                self.arduino.close()

        self.analog_inputs = line_converse

        return self.analog_inputs


    def readSerialPort(self, analog_input):
        """ Read port """
        t = threading.Thread(target=self.readSerialPortThread)
        t.daemon = True
        t.start()
        t.join()
        return self.analog_inputs[analog_input]


    def resetLEDS(self):

        GPIO.setmode(GPIO.BCM)
        GPIO.setup(2, GPIO.OUT)
        GPIO.setup(3, GPIO.OUT)
        GPIO.setup(17, GPIO.OUT)
        GPIO.setup(27, GPIO.OUT)
        self.red = GPIO.PWM(2, 100)
        self.blue = GPIO.PWM(3, 100)
        self.green = GPIO.PWM(27, 100)
        
        
        # self.red = GPIO.PWM(2, 0) 
        # self.blue = GPIO.PWM(3, 0.1) 
        # self.green = GPIO.PWM(4, 0) 


    def setLed(self,led, value):
        if value < 0 : value = 0
        if value > 100 : value = 100
        led.start(value)

    def settingRele(self):
        GPIO.setup(17, GPIO.OUT)

    def setRele(self, value):
        # if value > 50 : value = 1
        # else : value = 0
        GPIO.output(17, value)

    def __init__(self):
      
        self.resetLEDS()
        self.settingRele()
        station = StationDiY()
        station.login(username = "youruseremail", password = "yourpassword")
        
        while 1:

            try : 
                time.sleep(1)
                value = self.readSerialPort("A0")
                self.setLed(self.blue,1024-value)
                station.setSensor(device = "dev0", sensor = "t1", data = 1024-value)
                actioner_value = int(eval(station.getActionerData(device = "dev0", actioner = "a1"))["data"])
                actioner_value2 = int(eval(station.getActionerData(device = "dev0", actioner = "a0"))["data"])
                actioner_value3 = int(eval(station.getActionerData(device = "dev0", actioner = "a2"))["data"])
                self.setLed(self.green,actioner_value2)
                self.setLed(self.red,actioner_value)
                self.setRele(actioner_value3)

            except Exception as e : 
                pass 




if __name__ == '__main__':
  importer = Importer()
